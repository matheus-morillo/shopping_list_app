import 'dart:developer';

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: MyHomePage(title: 'Shopping list'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  final List<String> Shoppings = List<String>();
  final todoController = TextEditingController();

  @override
  Widget build(BuildContext context) {

    final List<Text> ShoppingsTextWidgets = new List<Text>();

    Shoppings.forEach((element) => ShoppingsTextWidgets.add(Text(element)));

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: [
          Row (
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                child: TextField (
                  decoration: InputDecoration(
                    hintText: 'Type your TODO',
                  ),
                  controller: todoController,
                ),
              ),
              RaisedButton(
                onPressed: () {
                  setState(() {
                    Shoppings.add(todoController.text);
                  });
                  todoController.text = '';
                },
                color: Colors.red[600],
                child: Icon(
                  Icons.more_time,
                  color: Colors.white,
                ),
              )
            ],
          ),
          Expanded(
            child: ListView(
              children: ShoppingsTextWidgets,
            ),
          )
        ],
      ),
    );
  }
}
